#/bin/bash
##############################
#
# Bash file to create input
# basis in linear format from
# cubic calculations
#
##############################
# Written by Martina Stella 
# and Valerio Vitale 
# November 2019 
##############################
#Input parameters
nocc=$1 # Number of occupied orbitals
nvirt=$2 # Number of virtual orbitals
natoms=$3 # Number of atoms

# Copy all files into target directory
cubic=$4
linear=$5
restart=$6

# Number of lines in SF header
nlines_linear=`awk -F ' ' '{print NF,NR}' ${linear}/minBasis-k001-NR.b000001 | grep '4 ' | head -2 | tail -1 | awk '/4/{print $2}'`
nlines_linear=$(( $nlines_linear - 1 ))

if [ -d "$restart" ]; then
   echo "$restart folder already exists!"
   exit
fi 

# Number of lines in the header of cubic
nlines=`echo $natoms+5|bc`
mkdir $restart

# Root names of occupied and virtual orbitals 
rootocc=`echo "wavefunction-k001-NR.b"`
rootvirt=`echo "virtuals-k001-NR.b"`

# Copying occupied orbitals down spin
for n in `seq 1 $nocc`; do 
	in=`echo $n | awk '{printf "%06i", $1}'`
        fname=`echo "$rootocc$in"`
        tname=`echo "minBasis-k001-NR.b$in"`
        echo $tname
        head -n $nlines_linear $linear/$tname > $restart/$tname
        cat $cubic/$fname | sed "1,$nlines d" >> $restart/$tname
done 

# Copying virtual orbitals down spin
for n in `seq 1 $nvirt`; do 
	in=`echo $n | awk '{printf "%06i", $1}'`
	iout=`echo $n | awk -v"occ=$nocc" '{printf "%06i", $1+occ}'`
        fname=`echo "$rootvirt$in"`
        tname=`echo "minBasis-k001-NR.b$iout"`
        head -n $nlines_linear $linear/$tname > $restart/$tname
        cat $cubic/$fname | sed "1,$nlines d" >> $restart/$tname
done

echo "Have a nice day!"
