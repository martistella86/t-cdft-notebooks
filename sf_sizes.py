# return a dictionary containing support function parameters where all SFs have the same radii
# currently for select elements only, to be generalized
def set_support_function_dict(rloc, rlock, basis_size):

    elements = ['H', 'C', 'N', 'O']

    # define the number of support functions for the given elements
    if basis_size == 's_sp':
        nsf = {'H': 1, 'C': 4, 'N': 4, 'O': 4}
        ig_dict = {'H': {'1s': 1},
                   'C': {'2s': 2, '2p': ['2/3', '2/3', '2/3']},
                   'O': {'2s': 2, '2p': ['4/3', '4/3', '4/3']},
                   'N': {'2s': 2, '2p': [1, 1, 1]}}

    elif basis_size == 'sp_spd':
        nsf = {'H': 4, 'C': 9, 'N': 9, 'O': 9}
        ig_dict = {'H': {'1s': 1, '2p': 0},
                   'C': {'2s': 2, '2p': ['2/3', '2/3', '2/3'], '3d': 0},
                   'O': {'2s': 2, '2p': ['4/3', '4/3', '4/3'], '3d': 0},
                   'N': {'2s': 2, '2p': [1, 1, 1], '3d': 0}}

    elif basis_size == 'ssp_spspd':
        nsf = {'H': 5, 'C': 13, 'N': 13, 'O': 13}
        ig_dict = {'H': {'1s': 1, '2s': 0, '2p': 0},
                   'C': {'2s': 2, '2p': ['2/3', '2/3', '2/3'], '3s': 0, '3p': 0, '3d': 0},
                   'O': {'2s': 2, '2p': ['4/3', '4/3', '4/3'], '3s': 0, '3p': 0, '3d': 0},
                   'N': {'2s': 2, '2p': [1, 1, 1], '3s': 0, '3p': 0, '3d': 0}}


    # not recommended for use for SF calculations
    # only used as a convenient means for varying the basis size for WFN T-CDFT calculations
    elif basis_size == 'sp_sp':
        nsf = {'H': 4, 'C': 4, 'N': 4, 'O': 4, 'Zn': 6}
        ig_dict = {'H': {'1s': 1, '2p': 0},
                   'C': {'2s': 2, '2p': ['2/3', '2/3', '2/3']},
                   'O': {'2s': 2, '2p': ['4/3', '4/3', '4/3']},
                   'N': {'2s': 2, '2p': [1, 1, 1]}}

    elif basis_size == 'ss_sp':
        nsf = {'H': 2, 'C': 4, 'N': 4, 'O': 4}
        ig_dict = {'H': {'1s': 1, '2s': 0},
                   'C': {'2s': 2, '2p': ['2/3', '2/3', '2/3']},
                   'O': {'2s': 2, '2p': ['4/3', '4/3', '4/3']},
                   'N': {'2s': 2, '2p': [1, 1, 1]}}

    elif basis_size == 'ssp_sp_ds':
        nsf = {'H': 5, 'C': 4, 'N': 4, 'O': 4}
        ig_dict = {'H': {'1s': 1, '2s': 0, '2p': 0},
                   'C': {'2s': 2, '2p': ['2/3', '2/3', '2/3']},
                   'O': {'2s': 2, '2p': ['4/3', '4/3', '4/3']},
                   'N': {'2s': 2, '2p': [1, 1, 1]}}

    elif basis_size == 's_spsp':
        nsf = {'H': 1, 'C': 8, 'N': 8, 'O': 8}
        ig_dict = {'H': {'1s': 1},
                   'C': {'2s': 2, '2p': ['2/3', '2/3', '2/3'], '3s': 0, '3p': 0},
                   'O': {'2s': 2, '2p': ['4/3', '4/3', '4/3'], '3s': 0, '3p': 0},
                   'N': {'2s': 2, '2p': [1, 1, 1], '3s': 0, '3p': 0}}

    elif basis_size == 'ss_spsp':
        nsf = {'H': 2, 'C': 8, 'N': 8, 'O': 8}
        ig_dict = {'H': {'1s': 1, '2s': 0},
                   'C': {'2s': 2, '2p': ['2/3', '2/3', '2/3'], '3s': 0, '3p': 0},
                   'O': {'2s': 2, '2p': ['4/3', '4/3', '4/3'], '3s': 0, '3p': 0},
                   'N': {'2s': 2, '2p': [1, 1, 1], '3s': 0, '3p': 0}}

    elif basis_size == 'ss_spspd':
        nsf = {'H': 2, 'C': 13, 'N': 13, 'O': 13}
        ig_dict = {'H': {'1s': 1, '2s': 0},
                   'C': {'2s': 2, '2p': ['2/3', '2/3', '2/3'], '3s': 0, '3p': 0, '3d': 0},
                   'O': {'2s': 2, '2p': ['4/3', '4/3', '4/3'], '3s': 0, '3p': 0, '3d': 0},
                   'N': {'2s': 2, '2p': [1, 1, 1], '3s': 0, '3p': 0, '3d': 0}}

    elif basis_size == 'ssp_spdf':
        nsf = {'H': 5, 'C': 16, 'N': 16, 'O': 16}
        ig_dict = {'H': {'1s': 1, '2s': 0, '2p': 0},
                   'C': {'2s': 2, '2p': ['2/3', '2/3', '2/3'], '3d': 0, '4f': 0},
                   'O': {'2s': 2, '2p': ['4/3', '4/3', '4/3'], '3d': 0, '4f': 0},
                   'N': {'2s': 2, '2p': [1, 1, 1], '3d': 0, '4f': 0}}

    elif basis_size == 'spd_spdf':
        nsf = {'H': 9, 'C': 16, 'N': 16, 'O': 16}
        ig_dict = {'H': {'1s': 1, '2p': 0, '3d': 0},
                   'C': {'2s': 2, '2p': ['2/3', '2/3', '2/3'], '3d': 0, '4f': 0},
                   'O': {'2s': 2, '2p': ['4/3', '4/3', '4/3'], '3d': 0, '4f': 0},
                   'N': {'2s': 2, '2p': [1, 1, 1], '3d': 0, '4f': 0}}

    elif basis_size == 'sspspd_spspdspd':
        nsf = {'H': 14, 'C': 22, 'N': 22, 'O': 22}
        ig_dict = {'H': {'1s': 1, '2s': 0, '2p': 0, '3s': 0, '3p': 0, '3d': 0},
                   'C': {'2s': 2, '2p': ['2/3', '2/3', '2/3'], '3s': 0, '3p': 0, '3d': 0, '4s': 0, '4p': 0, '4d': 0},
                   'O': {'2s': 2, '2p': ['4/3', '4/3', '4/3'], '3s': 0, '3p': 0, '3d': 0, '4s': 0, '4p': 0, '4d': 0},
                   'N': {'2s': 2, '2p': [1, 1, 1], '3s': 0, '3p': 0, '3d': 0, '4s': 0, '4p': 0, '4d': 0, '4f': 0}}

    elif basis_size == 'sspspd_spspdspdf':
        nsf = {'H': 14, 'C': 29, 'N': 29, 'O': 29, 'Zn': 14}
        ig_dict = {'H': {'1s': 1, '2s': 0, '2p': 0, '3s': 0, '3p': 0, '3d': 0},
                   'C': {'2s': 2, '2p': ['2/3', '2/3', '2/3'], '3s': 0, '3p': 0, '3d': 0, '4s': 0, '4p': 0, '4d': 0, '4f': 0},
                   'O': {'2s': 2, '2p': ['4/3', '4/3', '4/3'], '3s': 0, '3p': 0, '3d': 0, '4s': 0, '4p': 0, '4d': 0, '4f': 0},
                   'N': {'2s': 2, '2p': [1, 1, 1], '3s': 0, '3p': 0, '3d': 0, '4s': 0, '4p': 0, '4d': 0, '4f': 0}}
	
    else:
        print('Error in basis definition')
 
    lin_basis_dict = {'lin_basis_params': {'confinement': [-1, 0],
                  'rloc_kernel': rlock, 'rloc_kernel_foe': rlock}}

    for el in elements:
        lin_basis_dict['lin_basis_params'].update({el: {'nbasis': nsf[el], 'rloc': rloc}})

    lin_basis_dict.update({'ig_occupation': ig_dict})

    return lin_basis_dict



def get_number_of_sfs(log, sf_dict):
    nbasis = 0
    for atom in log.log['Properties of atoms in the system']:
        symbol = atom['Symbol']
        nat_spec = atom['No. of Atoms']
        nsf_per_spec = sf_dict['lin_basis_params'][symbol]['nbasis']
        nbasis += nsf_per_spec * nat_spec
    return nbasis
