# contains some useful routines and variables for making plots

import matplotlib.pyplot as plt
from matplotlib import gridspec
import matplotlib.colors as mpc
from matplotlib.offsetbox import (TextArea, DrawingArea, OffsetImage,
                                  AnnotationBbox)
from mpl_toolkits.axes_grid1 import make_axes_locatable

import matplotlib.ticker as ticker
import colours

# size to scale plots for displaying 
scale = 1.5

font_size = 14
fig_width = 3.5

colorso_rgb = [[143, 11, 11], [219, 0, 0], [255, 136, 51], [255, 194, 51]]
colorso = []
for rgb in colorso_rgb:
    html = colours.rgb_to_html(rgb)
    colorso.append(html)

colorsb_rgb = [[46, 0, 145], [42, 89, 219], [64, 172, 227], [119, 224, 230]]
colorsb = []
for rgb in colorsb_rgb:
    html = colours.rgb_to_html(rgb)
    colorsb.append(html)

markers = ['s', 'o', '^', 'v', 'd', 'p', 'h', 'D']
fs = 9


# function to simplify plotting
def set_plot_parameters(ax, xmin='DEFAULT', xmax='DEFAULT', ymin='DEFAULT', ymax='DEFAULT',
                        xlabel='DEFAULT', ylabel='DEFAULT', xtlabel='DEFAULT', ytlabel='DEFAULT',
                        xticklabels='DEFAULT', yticklabels='DEFAULT', xtickangle=0, ytickangle=0,
                        dx='DEFAULT', dy='DEFAULT', nmx='DEFAULT', nmy='DEFAULT',
                        shrink=1.0, key_loc='best', ncol=1, xloc=-0.05, labelpad=0.0, frameon=True,
                        handletextpad=0.2, handlelength=1.0, columnspacing=1.0, labelspacing=0.1, vs=1.03, borderpad=0.3):

    import numpy as np

    if xmin != 'DEFAULT':
        ax.set_xlim(xmin, xmax)
    if ymin != 'DEFAULT':
        ax.set_ylim(ymin, ymax)
    
    if xlabel != 'NONE':
        ax.set_xlabel(xlabel)
        
    if ylabel != 'NONE':
        ax.set_ylabel(ylabel)

    pad = 8
    mjl = 6
    mnl = 3

    if xtlabel == 'NOLABEL':
        label = False
    else:
        label = True
        
    if dx == 'NONE':
        ax.get_xaxis().set_ticks([])
    else:
        ax.tick_params(axis='x', which='major', direction='in', pad=pad, length=mjl,
                   bottom=True, top=True, labelbottom=label, labeltop=False)
        ax.tick_params(axis='x', which='minor', direction='in', pad=pad, length=mnl,
                   bottom=True, top=True, labelbottom=label, labeltop=False)

    if ytlabel == 'NOLABEL':
        label = False
    else:
        label = True
    
    if dy == 'NONE':
        ax.get_yaxis().set_ticks([])
    else:
        ax.tick_params(axis='y', which='major', direction='in', pad=pad, length=mjl,
                    left=True, right=True, labelleft=label, labelright=False)
        ax.tick_params(axis='y', which='minor', direction='in', pad=pad, length=mnl,
                   left=True, right=True, labelleft=label, labelright=False)
    
    if dx != 'DEFAULT' and dx != 'NONE':
        ax.xaxis.set_major_locator(ticker.MultipleLocator(dx))
        if nmx != 'DEFAULT':
            ax.xaxis.set_minor_locator(ticker.MultipleLocator(dx / nmx))
    if dy != 'DEFAULT' and dy != 'NONE':
        ax.yaxis.set_major_locator(ticker.MultipleLocator(dy))
        if nmy != 'DEFAULT':  
            ax.yaxis.set_minor_locator(ticker.MultipleLocator(dy / nmy))

    if xtickangle !=0 or xticklabels != 'DEFAULT':
        ax.set_xticks(np.arange(len(xticklabels)))
        ax.set_xticklabels(xticklabels, rotation=xtickangle) 

    if ytickangle !=0 or yticklabels != 'DEFAULT':
        ax.set_yticks(np.arange(len(yticklabels)))
        ax.set_yticklabels(yticklabels, rotation=ytickangle) 
    
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width, box.height * shrink])
    
    if key_loc == 'outside':
        ax.legend(loc='upper left', ncol=ncol, bbox_to_anchor=(xloc, vs), handletextpad=handletextpad, handlelength=handlelength,
                  columnspacing=columnspacing, labelspacing=labelspacing, frameon=frameon, borderpad=borderpad)
    elif key_loc == 'above':
        ax.legend(loc='lower center', ncol=ncol, bbox_to_anchor=(xloc, 1), handletextpad=handletextpad, handlelength=handlelength, columnspacing=columnspacing,
                  labelspacing=labelspacing, borderpad=borderpad)
    elif key_loc == 'abovec':
        ax.legend(loc='lower center', ncol=ncol, bbox_to_anchor=(0.5, 1), handletextpad=handletextpad, handlelength=handlelength, columnspacing=columnspacing,
                  labelspacing=labelspacing, borderpad=borderpad)
    elif key_loc == 'abover':
        ax.legend(loc='lower center', ncol=ncol, bbox_to_anchor=(0.36, 1), handletextpad=handletextpad, handlelength=handlelength, columnspacing=columnspacing,
                  labelspacing=labelspacing, borderpad=borderpad)
    elif key_loc != 'NONE':
        ax.legend(loc=key_loc, ncol=ncol, handletextpad=handletextpad, handlelength=handlelength, columnspacing=columnspacing, labelspacing=labelspacing, borderpad=borderpad)

    ax.xaxis.labelpad = labelpad
    ax.yaxis.labelpad = labelpad


