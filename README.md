# T-CDFT notebooks

This project provides the notebooks necessary for running the BigDFT calculations associated with the paper entitled _Transition-Based Constrained DFT for the Robust and Reliable Treatment of Excitations in Supramolecular Systems_, available [here](https://pubs.acs.org/doi/10.1021/acs.jctc.1c00548).

## Workflows

Workflows are available for the following types of calculation, using the notebooks as described. For each case, naphthalene has been used as an example.

### DeltaSCF

The notebook run\_wfn\_delta\_scf is a self-contained notebook for DeltaSCF calculations using cubic-scaling BigDFT. It runs the required ground state and excited state calculations.

### Wavefunction based TDDFT

The ground state wavefunctions must first be generated using the notebook generate\_wfn\_basis. In contrast to run\_wfn\_delta\_scf, a large number of virtual states should be requested to ensure the TDDFT calcalculation can be converged.

The TDDFT calculation is then performed via the notebook run\_wfn\_tddft, which also postprocesses the calculation to give both the energies and transition breakdown (purities).

Optionally, if the basis set used for TDDFT and T-CDFT is not the same, the transition breakdown should be used in conjunction with the get\_orbital\_overlap notebook, which calculates the overlap between the frontier orbitals coming from two different basis sets.

### Support function based TDDFT

The support function basis should be generated using generate\_sf\_basis, which performs both a standard linear scaling calculation, and a second non-self-consistent calculation to adapt the basis for the representaion of virtual states. If run\_wfn\_delta\_scf has already been, run the number of virtual states to include will correspond to the number of negative states in the corresponding ground state cubic scaling calculation.

The TDDFT calculation is then performed via the notebook run\_sf\_tddft, which, like run\_wfn\_tddft, also postprocesses the calculation to give both the energies and transition breakdown (purities).

Optionally, if the basis set used for TDDFT and T-CDFT is not the same, the transition breakdown should be used in conjunction with the get_orbital_overlap notebook, which calculates the overlap between the frontier orbitals coming from two different basis sets.

### Wavefunction based T-CDFT

As with WFN-based TDDFT, the ground state wavefunctions must first be generated using the notebook generate\_wfn\_basis. This must be run to completion, since the second half of the notebook will perform the necessary steps to convert the basis into support function format with effectively infinite localisation radii. 

The T-CDFT calculation is then performed using the notebook run\_pure\_wfn\_tcdft, which assumes a pure constraint.

Mixed T-CDFT calculations can instead be performed using the notebook run\_mixed\_wfn\_tcdft, using the same SF basis.

### Support function based T-CDFT

Similarly to SF-based TDDFT, the notebook generate\_sf\_basis should be used to generate the support function basis. 

The T-CDFT calculation is then performed using the notebook run\_pure\_sf\_tcdft, which assumes a pure constraint.

Mixed T-CDFT calculations can instead be performed using the notebook run\_mixed\_sf\_tcdft, using the same SF basis.

### Spatial Overlap

Cubic scaling BigDFT wavefunction files must first be generated for the ground state using the run\_wfn\_delta\_scf notebook as above. The notebook get\_spatial\_overlap may then be used to calculate the HOMO-LUMO spatial overlap using these files.

### Orbital Overlap

It is also possible to calculate the overlap between orbitals coming from a cubic scaling run, with those from a linear scaling run. This can be achieved using the notebook get\_orbital\_overlap.

## Results

In addition, this repository contains the notebooks and code outputs needed to reproduce various figures and tables from the paper. This can be achieved using the two notebooks plot\_frontier\_orbitals and plot\_excited\_states.

## Dependencies

This project uses the BigDFT code, which is available at https://gitlab.com/l_sim/bigdft-suite. Any issues can also be reported through the BigDFT GitLab page.
This project also uses cubetools.py (https://gist.github.com/aditya95sriram/8d1fccbb91dae93c4edf31cd6a22510f).

## Authors

Martina Stella,
Luigi Genovese,
Laura Ratcliff
