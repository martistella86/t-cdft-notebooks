# functions for extracting data from NWChem

import subprocess
import re


def get_homo_lumo_from_nwchem(nwfilename, na, occ_orbs, virt_orbs):

    import subprocess

    eig = {}
    for orb in occ_orbs + virt_orbs:
      eig[orb] = na

    COMMAND = "awk '/Alpha electrons/ {m=$4} END{printf(m)}' $1"
    nelec = int(subprocess.check_output([COMMAND, "sh", nwfilename], shell=True))

    nelec_init = nelec   

    for orb in occ_orbs:
    
        # this is really inelegant, but it works...
        if nelec > 9 and nelec < 100:
            COMMAND = "awk '/Vector   "+str(nelec)+"/ {m=$4} END{printf(m)}' $1"
            e = subprocess.check_output([COMMAND, "sh", nwfilename], shell=True)
        elif nelec > 99 and nelec < 1000:
            COMMAND = "awk '/Vector  "+str(nelec)+"/ {m=$4} END{printf(m)}' $1"
            e = subprocess.check_output([COMMAND, "sh", nwfilename], shell=True)
        e = e.decode('UTF-8')

        ei = e.split("=")
        e = ei[1].replace('D', 'E')
        #print orb, e
        eig[orb] = float(e)
    
        nelec -= 1
    
    nelec = nelec_init + 1

    for orb in virt_orbs:
    
        # this is really inelegant, but it works...
        if nelec > 9 and nelec < 100:
            COMMAND = "awk '/Vector   "+str(nelec)+"/ {m=$4} END{printf(m)}' $1"
            e = subprocess.check_output([COMMAND, "sh", nwfilename], shell=True)
            e = e.decode('UTF-8')
            # awkward case where eigenvalue is +ve and we have extra space
            if e == 'E=':
                COMMAND = "awk '/Vector   "+str(nelec)+"/ {m=$5} END{printf(m)}' $1"
                e = subprocess.check_output([COMMAND, "sh", nwfilename], shell=True)
                e = e.decode('UTF-8')
                ei = e
                e = ei.replace('D', 'E')
            else:
                ei = e.split("=")
                e = ei[1].replace('D', 'E')
        elif nelec > 99 and nelec < 1000:
            COMMAND = "awk '/Vector  "+str(nelec)+"/ {m=$4} END{printf(m)}' $1"
            e = subprocess.check_output([COMMAND, "sh", nwfilename], shell=True)
            e = e.decode('UTF-8')
            # awkward case where eigenvalue is +ve and we have extra space
            if e == 'E=':
                COMMAND = "awk '/Vector   "+str(nelec)+"/ {m=$5} END{printf(m)}' $1"
                ei = e
                e = ei.replace('D', 'E')
            else:
                ei = e.split("=")
                e = ei[1].replace('D', 'E')

        eig[orb] = float(e)
    
        nelec += 1

    return eig


def get_etd_from_nwchem(nwfilename, na, state, nroot):

    energy = []
    
    e = na
    ns = 0
    
    for n in range(1, nroot + 1):
        if n > 9:
            str2search = "Root  "+str(n)
        else:
            str2search = "Root   "+str(n)
            
        COMMAND = "awk '/"+str2search+"/ {m=$6} END{printf(m)}' $1"
        e = subprocess.check_output([COMMAND,"sh", nwfilename],shell=True)
        COMMAND = "grep -A1 '"+str2search+"' "+nwfilename+" | tail -n1 | awk '{printf($3)}'"
        spinb = subprocess.check_output([COMMAND],shell=True)
        spin = spinb.decode('UTF-8')
        
        if state[0] == 'S':
            if spin[0:5] == "0.000":
                ns += 1
                if ns == int(state[1]):
                    return float(e)
            elif spin[0:5] == "-0.000":
                ns += 1
                if ns == int(state[1]):
                    return float(e)

        elif state[0] == 'T':
            if spin[0:5] == "2.000":
                ns += 1
                if ns == int(state[1]):
                    return float(e)


